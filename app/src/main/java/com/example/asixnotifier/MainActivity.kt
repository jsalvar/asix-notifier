package com.example.asixnotifier

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.example.asixnotifier.fragments.ListNotificationFragment
import com.example.asixnotifier.helpers.FragmentManagerHelper

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FragmentManagerHelper.buildFragment(supportFragmentManager, ListNotificationFragment(), "listNotifications", false)
    }

    override fun onBackPressed() {
        val supportFragmentManager: FragmentManager = supportFragmentManager

        if (supportFragmentManager.backStackEntryCount > 0) supportFragmentManager.popBackStackImmediate()
        else super.onBackPressed()
    }
}

