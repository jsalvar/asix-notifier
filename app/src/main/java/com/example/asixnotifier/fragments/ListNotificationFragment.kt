package com.example.asixnotifier.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.example.asixnotifier.R
import com.example.asixnotifier.helpers.AdapterOnClickListener
import com.example.asixnotifier.helpers.FragmentManagerHelper
import com.example.asixnotifier.helpers.NotificationsListAdapter
import com.example.asixnotifier.helpers.RestApiHelper
import com.example.asixnotifier.models.Notification
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONArray
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ListNotificationFragment : Fragment() {
    private val restApiHelper = RestApiHelper()

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View
    {
        val rootView : View = inflater.inflate(R.layout.list_notification_fragment, container, false)

        val listener = Response.Listener<JSONArray> { response ->
            run {
                val notifications : ArrayList<Notification> = ArrayList()

                for (i in 0 until response.length()) {
                    val notification = response.getJSONObject(i)

                    notifications.add(
                        Notification(
                            notification.getInt("id"),
                            notification.getString("subject"),
                            notification.getString("message"),
                            SimpleDateFormat("dd/MM/yyyy").format(Date(notification.getLong("created_at") * 1000L))
                        )
                    )
                }

                var notificationsList : RecyclerView? = rootView.findViewById(R.id.notifications_list)
                var layoutManager : RecyclerView.LayoutManager? =  LinearLayoutManager(requireContext().applicationContext)
                var adapter : NotificationsListAdapter? = NotificationsListAdapter(
                    notifications,
                    object : AdapterOnClickListener {
                        override fun onClick(view : View, position : Int) {
                            FragmentManagerHelper.buildFragment(requireActivity().supportFragmentManager, ShowNotificationFragment(notifications[position].id), "showNotification",true)
                        }
                    }
                )

                notificationsList?.layoutManager = layoutManager
                notificationsList?.adapter = adapter
            }
        }

        val errorListener = Response.ErrorListener { error ->
            run {
                Log.e("N_REQUEST_ERROR", error.message ?: "Null message")
            }
        }

        restApiHelper.sendJsonArrayRequest(
            requireContext().applicationContext,
            Request.Method.GET,
            "notifications",
            null,
            listener,
            errorListener
        )

        val createNotificationButton = rootView.findViewById<FloatingActionButton>(R.id.create_notification_button);
        createNotificationButton.setOnClickListener {
            FragmentManagerHelper.buildFragment(requireActivity().supportFragmentManager, CreateNotificationFragment(), "createNotification",true)
        }

        return rootView
    }
}
