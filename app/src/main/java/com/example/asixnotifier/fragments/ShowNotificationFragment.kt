package com.example.asixnotifier.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.example.asixnotifier.R
import com.example.asixnotifier.helpers.RestApiHelper
import com.example.asixnotifier.models.Notification
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class ShowNotificationFragment(notificationId : Int) : Fragment() {
    private val notificationId : Int = notificationId

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {
        val rootView : View = inflater.inflate(R.layout.show_notification_fragment, container, false)

        rootView.visibility = View.INVISIBLE

        val restApiHelper =  RestApiHelper()

        val params = JSONObject()
        params.put("id", this.notificationId)

        val listener =  Response.Listener<JSONObject> { response ->
            run {
                val notification = Notification(
                    response.getInt("id"),
                    response.getString("subject"),
                    response.getString("message"),
                    SimpleDateFormat("dd/MM/yyyy").format(
                        Date(response.getLong("created_at") * 1000L)
                    )
                )

                val notificationSubjectText = rootView.findViewById<TextView>(R.id.notification_subject_text)
                val notificationMessageText = rootView.findViewById<TextView>(R.id.notification_message_text)
                val notificationCreatedAtText = rootView.findViewById<TextView>(R.id.notification_created_at_text)

                if (notification.subject !== "null" ) notificationSubjectText.text = notification.subject
                else notificationSubjectText.text = "Sin asunto"

                notificationMessageText.text = notification.message
                notificationCreatedAtText.text = notification.createdAt

                rootView.visibility = View.VISIBLE
            }
        }

        val errorListener = Response.ErrorListener { error ->
            run {
                Log.e("N_SHOW_ERROR", error.message ?: "Null message")
            }
        }

        restApiHelper.sendJsonObjectRequest(
            requireContext().applicationContext,
            Request.Method.GET,
            "notifications/${this.notificationId}",
            params,
            listener,
            errorListener
        )

        return rootView
    }
}