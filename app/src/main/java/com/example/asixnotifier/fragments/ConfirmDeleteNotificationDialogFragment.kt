package com.example.asixnotifier.fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.android.volley.Request
import com.android.volley.Response
import com.example.asixnotifier.R
import com.example.asixnotifier.helpers.NotificationsListAdapter
import com.example.asixnotifier.helpers.RestApiHelper
import org.json.JSONObject

class ConfirmDeleteNotificationDialogFragment(notificationId : Int, adapter : NotificationsListAdapter, currentItemPosition : Int) : DialogFragment() {
    private val notificationId : Int = notificationId
    private val adapter : NotificationsListAdapter = adapter
    private val currentItemPosition : Int = currentItemPosition

    override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.delete_notification_dialog_text)
                .setPositiveButton(R.string.delete_notification_dialog_yes) { _, _ ->
                    run {
                        val restApiHelper = RestApiHelper()

                        val params = JSONObject()
                        params.put("id", this.notificationId)

                        val listener =  Response.Listener<JSONObject> {
                            this.adapter.notifications?.removeAt(this.currentItemPosition)
                            this.adapter.notifyItemRemoved(this.currentItemPosition)
                            this.adapter.notifyItemRangeChanged(this.currentItemPosition, this.adapter.notifications!!.size)
                        }

                        val errorListener = Response.ErrorListener { error ->
                            run {
                                Log.e("N_DELETE_ERROR", error.message ?: "Null message")
                            }
                        }

                        restApiHelper.sendJsonObjectRequest(
                            requireContext().applicationContext,
                            Request.Method.DELETE,
                            "notifications/${this.notificationId}",
                            params,
                            listener,
                            errorListener
                        )
                    }
                }
                .setNegativeButton(R.string.delete_notification_dialog_no, null)

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}