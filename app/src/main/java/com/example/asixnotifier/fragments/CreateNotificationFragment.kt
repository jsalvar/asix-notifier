package com.example.asixnotifier.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.example.asixnotifier.R
import com.example.asixnotifier.helpers.FragmentManagerHelper
import com.example.asixnotifier.helpers.RestApiHelper
import org.json.JSONObject

class CreateNotificationFragment : Fragment() {
    private fun View.hideKeyboard() {
        val inputManager : InputMethodManager = requireContext().applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View {
        val rootView : View = inflater.inflate(R.layout.create_notification_fragment, container, false)

        val submitAmazonNotificationButton = rootView.findViewById<Button>(R.id.notification_submit_button)
        submitAmazonNotificationButton.setOnClickListener {
            rootView.visibility = View.INVISIBLE

            this.storeNotification(rootView)

            it.hideKeyboard()
        }

        return rootView
    }

    private fun storeNotification(rootView : View) {
        val restApiHelper = RestApiHelper()

        val subject = rootView.findViewById<EditText>(R.id.notification_subject_input)
        val message = rootView.findViewById<EditText>(R.id.notification_message_input)

        val params = JSONObject()
        params.put("subject", subject.text.toString())
        params.put("message", message.text.toString())

        val listener = Response.Listener<JSONObject> {
            FragmentManagerHelper.buildFragment(requireActivity().supportFragmentManager, ListNotificationFragment(), "listNotification", true)
        }

        val errorListener = Response.ErrorListener { error ->
            run {
                Log.e("N_CREATE_ERROR", error.message ?: "Null message")
            }
        }

        restApiHelper.sendJsonObjectRequest(
            requireContext().applicationContext,
            Request.Method.POST,
            "notifications",
            params,
            listener,
            errorListener
        )
    }
}