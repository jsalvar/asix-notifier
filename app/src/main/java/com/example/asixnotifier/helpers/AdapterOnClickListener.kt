package com.example.asixnotifier.helpers

import android.view.View

interface AdapterOnClickListener {
    fun onClick(view : View, position : Int)
}