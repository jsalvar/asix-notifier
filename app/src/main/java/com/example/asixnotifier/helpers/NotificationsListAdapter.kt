package com.example.asixnotifier.helpers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.asixnotifier.R
import com.example.asixnotifier.fragments.ConfirmDeleteNotificationDialogFragment
import com.example.asixnotifier.models.Notification
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.list_notification_template.view.*

class NotificationsListAdapter(notifications : ArrayList<Notification>, private var onClickListener : AdapterOnClickListener) : RecyclerView.Adapter<NotificationsListAdapter.ViewHolder>() {
    var notifications : ArrayList<Notification>? = null

    private var viewHolder : ViewHolder? = null

    init {
        this.notifications = notifications
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.list_notification_template, parent, false)

        this.viewHolder = ViewHolder(view, onClickListener)

        return this.viewHolder!!
    }

    override fun getItemCount() : Int {
        return this.notifications?.count()!!
    }

    override fun onBindViewHolder(holder : ViewHolder, position : Int) {
        val notification : Notification? = this.notifications?.get(position)

        if (notification?.subject !== "null") holder.subject?.text = (notification?.subject)
        else holder.subject?.text = "Sin asunto"

        holder.deleteNotificationButton?.setOnClickListener {
            val fragmentManager : FragmentManager = (holder.itemView.context as FragmentActivity).supportFragmentManager
            val deleteDialogFragment = ConfirmDeleteNotificationDialogFragment(notification!!.id, this, position)

            deleteDialogFragment.show(fragmentManager, "deleteNotification")
        }
    }

    class ViewHolder(view : View, onClickListener : AdapterOnClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
        var view = view
        var subject : TextView? = null
        var deleteNotificationButton : FloatingActionButton? = null

        private var onClickListener : AdapterOnClickListener? = null

        init {
            this.subject = view.notification_subject
            this.deleteNotificationButton = view.delete_notification_button

            this.onClickListener = onClickListener

            view.setOnClickListener(this)
        }

        override fun onClick(view : View?) {
            this.onClickListener?.onClick(view!!, adapterPosition)
        }
    }
}