package com.example.asixnotifier.helpers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.asixnotifier.R

class FragmentManagerHelper {
    companion object {
        fun buildFragment(fragmentManager : FragmentManager, substituteFragment : Fragment, tag : String, stackable : Boolean
        ) {
            val fragmentTransaction  : FragmentTransaction = fragmentManager.beginTransaction()

            fragmentTransaction.replace(R.id.fragment, substituteFragment, tag)
            if (stackable) fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }
}
