package com.example.asixnotifier.helpers

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

class RestApiHelper {
    private val uri : String = "http://10.0.2.2:80/asix-notifier/public/api/";

    private fun getUri() : String {
        return this.uri;
    }

    fun sendJsonArrayRequest(
        context : Context?,
        method : Int,
        endpoint : String,
        params : JSONArray?,
        listener : Response.Listener<JSONArray>?,
        errorListener : Response.ErrorListener?
    ) {
        val queue : RequestQueue = Volley.newRequestQueue(context)

        val jsonArrayRequest = JsonArrayRequest(
            method,
            this.getUri() + endpoint,
            params,
            listener,
            errorListener
        )

        queue.add(jsonArrayRequest)
    }

    fun sendJsonObjectRequest(
        context : Context?,
        method : Int,
        endpoint : String,
        params : JSONObject?,
        listener : Response.Listener<JSONObject>?,
        errorListener : Response.ErrorListener?
    ) {
        val queue : RequestQueue = Volley.newRequestQueue(context)

        val jsonObjectRequest = JsonObjectRequest(
            method,
            this.getUri() + endpoint,
            params,
            listener,
            errorListener
        )

        queue.add(jsonObjectRequest)
    }
}