package com.example.asixnotifier.models

class Notification(id : Int, subject : String, message : String, createdAt : String) {
    var id : Int = 0
    var subject : String = ""
    var message : String = ""
    var createdAt : String = ""

    init {
        this.id = id
        this.subject = subject
        this.message = message
        this.createdAt = createdAt
    }
}